#!/bin/bash
#SBATCH --partition=exacloud
#SBATCH--qos long_jobs 
#SBATCH -A chin_lab
#SBATCH --mem 128G
#SBATCH --time 9-0
srun python -u "PythonScripName" $1