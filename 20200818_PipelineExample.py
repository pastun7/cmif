# image processing for with mlpex_image
# date: 2020-08-18
# author: engje
# language Python 3.8
# license: GPL>=v3

#libraries

import os
import sys
import numpy as np
import pandas as pd
import shutil
import matplotlib.pyplot as plt
import re
from skimage import io

# cd /home/groups/graylab_share/OMERO.rdsStore/engje/Data/cmIF

####  Paths  ####

codedir = os.getcwd()
rootdir = f'{codedir}/data/PipelineExample'
#czidir = rootdir.replace('_Workflow','_Images')
czidir = f'{rootdir}/czis'

#automatically generated
tiffdir = f'{rootdir}/RawImages'
qcdir = f'{rootdir}/QC'
regdir = f'{rootdir}/RegisteredImages'
subdir = f'{rootdir}/SubtractedRegisteredImages'
segdir = f'{rootdir}/Segmentation'
cropdir = f'{rootdir}/Cropped'


# Start Preprocessing

from mplex_image import preprocess, mpimage, cmif
preprocess.cmif_mkdir([tiffdir,qcdir,regdir,segdir,subdir,cropdir])

ls_sample = ['BC44290-146'
 ]
 
#### 1 QC filenames and export metadata from .czi #### 
'''
from mplex_image import metadata
import javabridge
import bioformats
javabridge.start_vm(class_path=bioformats.JARS)
for s_sample in ls_sample:
    os.chdir(f'{czidir}/{s_sample}')
    #1 rename undescores to dot to match convention (done)
    d_rename = mpimage.underscore_to_dot(s_sample,s_end='.czi')
    d_rename.update({'HER2_ER':'HER2.ER'})
    preprocess.dchange_fname(d_rename) #,b_test=False)

    #2 Check files/naming
    df_img = cmif.parse_czi(f'{czidir}/{s_sample}',b_scenes=True)
    cmif.count_images(df_img)
    preprocess.check_names(df_img,s_type='czi')
    #Example: change file name and change back
    d_rename = {'CK5R':'CK5Rename','CK5Rename':'CK5R'} 
    preprocess.dchange_fname(d_rename)#,b_test=False)
    
    #3 Export useful imaging metadata (done)
    df_img = metadata.scene_position(f'{czidir}/{s_sample}',type='r')
    #df_img.to_csv(f'{codedir}/{s_sample}_ScenePositions.csv')
    metadata.exposure_times_scenes(df_img,rootdir,czidir=f'{czidir}/{s_sample}',s_end='.czi')#
javabridge.kill_vm()
'''
#### 2 Raw tiffs: check/change names ####
'''
for s_sample in ls_sample: 
    os.chdir(f'{tiffdir}/{s_sample}')
    #Example: change file name and change back
    d_rename = {'CK5R':'CK5Rename','CK5Rename':'CK5R'} 
    preprocess.dchange_fname(d_rename) #,b_test=False)
    #sort and count images 
    df_img = mpimage.parse_org(s_end = "ORG.tif",type='raw') 
    cmif.count_images(df_img[df_img.slide==s_sample])
'''
#### 3 QC Raw tiffs: visual inspection ####
'''
preprocess.cmif_mkdir([f'{qcdir}/RawImages'])

for s_sample in ls_sample:
    os.chdir(f'{tiffdir}/{s_sample}')
    #investigate tissues
    df_img = mpimage.parse_org(s_end="ORG.tif",type='raw')
    cmif.visualize_raw_images(df_img,qcdir,color='c1')
'''

#### 4 Register ####
'''
for s_sample in ls_sample:
    cmif.registration_python(s_sample,tiffdir,regdir,qcdir)
'''
#### 5 Check Registration Visualization #### 
'''
cmif.visualize_reg_images(f'{regdir}',qcdir,color='c1')
'''
#### 6 Create AF Subtracted Images #### 
'''
#parameters
d_channel = {'c2':'R8Qc2','c3':'R8Qc3','c4':'R8Qc4','c5':'R8Qc5'}
d_early={'c2':'R0c2','c3':'R0c3','c4':'R0c4','c5':'R0c5'}

for s_sample in ls_sample:
    preprocess.cmif_mkdir([f'{subdir}/{s_sample}'])
    os.chdir(f'{regdir}')
    for s_file in os.listdir():
        if s_file.find(s_sample) > -1:
            os.chdir(s_file)
            df_img = mpimage.parse_org()
            ls_exclude = sorted(set(df_img[df_img.color=='c5'].marker)) + ['DAPI'] + [item for key, item in d_channel.items()] + [item for key, item in d_early.items()]
            #subtract
            df_markers = cmif.autofluorescence_subtract(s_sample,df_img,f'{codedir}/data/PipelineExample',d_channel,ls_exclude,subdir=f'{subdir}/{s_sample}',d_early=d_early) #

#generate channel/marker metadata csv
cmif.metadata_table(regdir,segdir)
'''
#### 7 Cellpose segmentation ####
'''
from mplex_image import segment
import time

nuc_diam = 30
cell_diam = 30 

s_seg_markers = "['Ecad']"
s_type = 'nuclei'#'cell' #

print(f'Predicting {s_type}')
for s_sample in ls_sample:
    preprocess.cmif_mkdir([f'{segdir}/{s_sample}Cellpose_Segmentation'])
    os.chdir(f'{regdir}')
    for s_file in os.listdir():
        if s_file.find(s_sample) > -1:
            os.chdir(f'{regdir}/{s_file}')
            print(f'Processing {s_file}')
            df_img = segment.parse_org()
            for s_scene in sorted(set(df_img.scene)):
                s_slide_scene= f'{s_sample}-Scene-{s_scene}'
                s_find = df_img[(df_img.rounds=='R1') & (df_img.color=='c1') & (df_img.scene==s_scene)].index[0]
                segment.cellpose_segment_job(s_file,s_slide_scene,
                 s_find,f'{segdir}/{s_sample}Cellpose_Segmentation',
                 f'{regdir}/{s_slide_scene}',nuc_diam,cell_diam,
                 s_type,s_seg_markers)#,b_match=True) #b_long=True)
                os.chdir(f'{segdir}/{s_sample}Cellpose_Segmentation')
                os.system(f'sbatch cellpose_{s_type}_{s_slide_scene}.sh')
                time.sleep(9)
                print('Next')
'''

#### 8 Extract Cellpose Features ####
'''
from mplex_image import features

nuc_diam = 30
cell_diam = 30 
ls_seg_markers = ['Ecad']

for s_sample in ls_sample: 
    df_sample, df_thresh = features.extract_cellpose_features(s_sample, segdir, subdir, ls_seg_markers, nuc_diam, cell_diam)
    df_sample.to_csv(f'{segdir}/features_{s_sample}_MeanIntensity_Centroid_Shape.csv')
    df_thresh.to_csv(f'{segdir}/thresh_{s_sample}_ThresholdLi.csv')
'''

#### 9 Filter cellpose features #### 
'''
from mplex_image import process, features
#parameters
nuc_diam = 30
cell_diam = 30 
ls_seg_markers = ['Ecad']
ls_marker_cyto = ['CK14_cytoplasm','CK5_cytoplasm','CK17_cytoplasm','CK19_cytoplasm','CK7_cytoplasm','CK5R_cytoplasm','Ecad_cytoplasm','HER2_cytoplasm']
ls_custom = ['CD20R_perinuc5','CD4R_perinuc5','CD8R_perinuc5']
ls_filter = ['DAPI8Q_nuclei','DAPI2_nuclei']
man_thresh = 600

#filtering
for s_sample in ls_sample: 
    os.chdir(segdir)
    df_img_all = process.load_li([s_sample])
    df_mi_full = process.load_cellpose_df([s_sample], segdir)
    df_xy = process.filter_cellpose_xy(df_mi_full)
    #manually override too low Ecad thresh
    df_img_all.loc[df_img_all[(df_img_all.marker=='Ecad') & (df_img_all.threshold_li < man_thresh)].index, 'threshold_li'] = man_thresh
    df_mi_filled = process.fill_cellpose_nas(df_img_all,df_mi_full,ls_marker_cyto,s_thresh='Ecad',ls_celline=[],ls_shrunk = ['CD44','Vim'],qcdir=qcdir)
    df_mi = process.filter_loc_cellpose(df_mi_filled, ls_marker_cyto, ls_custom)
    df_pos_auto,d_thresh_record = process.auto_threshold(df_mi,df_img_all)
    ls_color = [item + '_nuclei' for item in df_img_all[(df_img_all.slide_scene==df_img_all.slide_scene.unique()[0]) & (df_img_all.marker.str.contains('DAPI'))].marker.tolist()]
    process.positive_scatterplots(df_pos_auto,d_thresh_record,df_xy,ls_color + ['Ecad_cytoplasm'],qcdir)
    df_mi_filter = process.filter_dapi_cellpose(df_pos_auto,ls_color,df_mi,ls_filter,df_img_all,qcdir)
    df_mi_filter.to_csv(f'{segdir}/features_{s_sample}_FilteredMeanIntensity_{"_".join([item.split("_")[0] for item in ls_filter])}.csv')
    df_xy.to_csv(f'{segdir}/features_{s_sample}_CentroidXY.csv')
    #Expand nuclei without matching cell labels for cells touching analysis
    se_neg = df_mi_full[df_mi_full.slide == s_sample].Ecad_negative
    labels,combine,dd_result = features.combine_labels(s_sample, segdir, subdir, ls_seg_markers, nuc_diam, cell_diam, se_neg)

#make a nice rounds/channels/markers table
df_img_all['round_ord'] = [re.sub('Q','.5', item) for item in df_img_all.rounds]
df_img_all['round'] = [float(re.sub('[^0-9.]','', item)) for item in df_img_all.round_ord]
df_marker = df_img_all[(df_img_all.slide_scene==df_img_all.slide_scene.unique()[0])].loc[:,['marker','round','color']].pivot('round','color')
df_marker.index.name = None
df_marker.to_csv(f'{qcdir}/MarkerTable.csv',header=None)
'''

#### 10 generate multicolor pngs and ome-tiff overlays (cropped) #### 
'''
#crop coordinates  x, y upper corner
d_crop = {
 #'BC44290-146-Scene-1': (2000,7000),
 #'BC44290-146-Scene-1': (2000,5000),
 'BC44290-146-Scene-1': (1800,9000)
 }
tu_dim=(2000,2000)

#10-1 PNGs
#PNG parameters
d_overlay = {'R1':['CD20','CD8','CD4','CK19'],
     'R2':[ 'PCNA','HER2','ER','CD45'],
     'R3':['pHH3', 'CK14', 'CD44', 'CK5'],
     'R4':[ 'Vim', 'CK7', 'PD1', 'LamAC',],
     'R5':['aSMA', 'CD68', 'Ki67', 'Ecad'],
     'R6':['CK17','PDPN','CD31','CD3'],
     'R7':['CK5R','CD8R','CD4R','CD20R'],
     'R8':['LamB1','AR','ColIV','ColI'],
     'subtype':['PCNA','HER2','ER','Ki67'],
     'diff':['Ecad', 'CK14', 'CD44', 'CK5'],
     'immune':['PD1','CD8R','CD4R','CD20R'],
     'stromal':['aSMA','Vim','CD68','CD31'],
     }
es_bright = {'pHH3','CD68','CK14','CK5','CK17'} 

for s_sample in ls_sample:
    preprocess.cmif_mkdir([f'{qcdir}/{s_sample}'])
    s_path = f'{subdir}/{s_sample}'
    os.chdir(s_path)
    df_img = mpimage.parse_org()
    df_img['path'] = [f'{s_path}/{item}' for item in df_img.index]
    df_dapi_round = df_img[(df_img.color=='c1')&(df_img.scene==s_scene) & (df_img.rounds=='R2')]
    df_scene = df_img[(df_img.color!='c1') & (df_img.scene==s_scene)]
    for s_round in ['subtype','immune','diff','stromal']:
        df_round = df_scene[df_scene.marker.isin(d_overlay[s_round])]
        high_thresh=0.999
        d_overlay_round = {s_round:d_overlay[s_round]}
        d_result = mpimage.multicolor_png(df_round,df_dapi_round,s_scene=s_scene,d_overlay=d_overlay_round,d_crop=d_crop,es_dim={'nada'},es_bright=es_bright,low_thresh=2000,high_thresh=high_thresh)
        for key, tu_result in d_result.items():
            io.imsave(f'{qcdir}/{s_sample}/ColorArray_{s_scene}_{key}_{".".join(tu_result[0])}.png',tu_result[1])


#10 -2 ome-tiff
import tifffile
#ome-tiff parameters
s_dapi = 'DAPI1'
d_combos = {'AF':{'R0c2','R0c3','R0c4','R0c5'}, 
        'Stromal':{'PDPN','Vim','CD31','aSMA'}, 
        'Tumor':{'HER2','ER','AR','Ecad','CD44','Ki67','pHH3','PCNA'},
        'Immune':{'CD45','CD20R','CD68','PD1', 'CD8R', 'CD4R','CD3',},
        'Differentiation':{'CK7','CK19','CK14','CK17','CK5','CD44','Vim'},
        'Nuclear_ImmuneEarly':{ 'LamB1', 'LamAC','CD20','CD8', 'CD4',},
    }
for s_scene in sorted(d_crop.keys()):
    s_sample = s_scene.split('-Scene')[0]
    os.chdir(f'{subdir}/{s_sample}')
    df_img = mpimage.parse_org()
    d_crop_scene = {s_scene:d_crop[s_scene]}
    dd_result = mpimage.overlay_crop(d_combos,d_crop,df_img,s_dapi,tu_dim)
    for s_crop, d_result in dd_result.items():
        for s_type, (ls_marker, array) in d_result.items():
            new_array = array[np.newaxis,np.newaxis,:]
            s_xml =  mpimage.gen_xml(new_array, ls_marker)
            with tifffile.TiffWriter(f'{cropdir}/{s_crop}_{s_type}.ome.tif') as tif:
                tif.save(new_array,  photometric = "minisblack", description=s_xml, metadata = None)

#10-3 crop basins to match cropped overlays

cmif.crop_labels(d_crop,tu_dim,segdir,cropdir,s_find='exp5_CellSegmentationBasins')
cmif.crop_labels(d_crop,tu_dim,segdir,cropdir,s_find='Nuclei Segmentation Basins')
'''

#### 11 Tissue edge detection ####
'''
from mplex_image import features
nuc_diam = 30
i_pixel = 308
for s_sample in ls_sample:
    features.edge_mask(s_sample,segdir,subdir,i_pixel=i_pixel, dapi_thresh=350)
    df_sample = features.edge_cells(s_sample,segdir,nuc_diam,i_pixel=i_pixel)
    df_sample.to_csv(f'{segdir}/features_{s_sample}_EdgeCells{i_pixel}pixels_CentroidXY.csv')
'''

os.chdir(codedir)

