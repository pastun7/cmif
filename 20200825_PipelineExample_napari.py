# 2020-05-05 Need to threshold with Napari!
# author engje
# ipython --gui=qt
#%run 20200504_JPTMAs_napari.py
import napari
import os
import skimage
from skimage import io
import numpy as np
import copy
import pandas as pd
import tifffile

#paths
codedir = 'C:\\Users\\engje\\Documents\\Data\\PipelineExample'
s_slide = 'BC44290-146-Scene-1'
regdir = f'{codedir}\\44290-146_Cropped'

os.chdir('..')
from cmif.mplex_image import visualize as viz
from cmif.mplex_image import process

#load positive and intensity data
os.chdir(codedir)
df_mi = pd.read_csv(f'features_BC44290-146_FilteredMeanIntensity_DAPI8Q_DAPI2.csv',index_col=0)
df_pos = pd.read_csv(f'20200826_BC44290-146_ManualPositive.csv',index_col=0)

#load images
os.chdir(regdir)
s_crop = 'x1800y9000'
s_crop = 'x2200y5500'
s_crop = 'x1800y3500'#out of focus 
viewer = napari.Viewer()
label_image = viz.load_crops(viewer,ls_crop)

#show positive results
ls_cell = ['CD8R', 'CD4_','CD3_',
    'ER', 'PCNA','AR'
    #'HER2','Ecad',
    #'R0c2','R8Qc2'
  ]

df_scene = df_pos[df_pos.slide_scene==f"{s_slide.split('-Scene-')[0]}_scene{s_slide.split('-Scene-')[1]}"]

for s_cell in ls_cell: 
    label_image_cell = viz.pos_label(viewer,df_scene,label_image,s_cell)

os.chdir(codedir)


